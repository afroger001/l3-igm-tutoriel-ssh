# Accès à distance en mode bureau avec X2Go Client


Cette partie vous explique comment vous pouvez accéder à votre compte au CREMI en mode bureau, c'est à dire en affichant (graphiquement) l’intégralité de votre bureau CREMI sur votre machine locale.

!!! attention
    Cet accès en mode bureau sollicite durement le réseau en terme de traffic et peut de mener à des congestions si tous les étudiants en font une utilisation trop régulière. Il faut donc l'utiliser avec parcimonie quand il n'y a pas de solutions alternatives appropriées.

## Installation


=== "Windows et macOS"

    * Se rendre sur la page Wiki de X2Go : <https://wiki.x2go.org/doku.php> 
    * Télécharger X2Go Client en cliquant sur le lien correspondant à votre système d’exploitation dans le cadre `GetX2Go Client` (ligne `Download X2Go Client`)
    ![image x2go](images/X2Go-1.png)
    * Effectuer l’installation en exécutant le fichier téléchargé 

=== "Linux"

    * Ouvrir un terminal
    * Exécuter une à une les commandes suivantes pour installer X2Go Client
    ``` bash
    sudo apt-add-repository ppa:x2go/stable
    sudo apt-get update
    sudo apt-get install x2goclient 
    ```

---

## Utilisation

* Lancer X2Go Client (en double-cliquant sur l’icône créé sur le bureau ou dans la liste des applications)

* Remplir les champs suivants :
    * Nom de la session : `CREMI` (le nom de session est libre)
    * Hôte : `NomMachine` (parmi la liste suivante : `boursouf`, `boursouflet`, `cocatrix`, `trelawney`, `mcgonagall`, `joli-coeur`, `infini1`, `infini2`, `xeonphi`, `tesla`)
    * Identifiant : `username` ( ⚠️ __à remplacer par votre nom d'utilisateur CREMI__ ⚠️)
    
    
* Cocher la case *Utiliser un serveur mandataire pour la connexion SSH* et remplissez le rectangle
qui apparaît comme suit :
    * Hôte : `sshproxy.emi.u-bordeaux.fr`
    * Cocher `Même identifiant que le serveur X2Go`
    * Cocher `Même mot de passe que sur le serveur X2Go`

* Dans le rectangle *Type de session* (tout en bas), choisir `XFCE`.

??? info "Afficher l'écran rempli"
    L’écran suivant apparaît :
    ![image x2go](images/X2Go-CREMI.png)    
	    
* Cliquer sur OK.
* Cliquer sur le rectangle à droite intitulé `CREMI` (ou le nom de la session choisi)
* Entrez votre mot de passe habituel et cliquer sur OK. Une fenêtre va s'ouvrir avec votre bureau comme lors d'une connexion sur un ordinateur du CREMI.


!!! danger "Il faut quitter sa session proprement !"
    Quand vous avez terminé, quittez votre session comme vous le faites habituellement (en cliquant sur votre nom en haut à droite, puis sur `Déconnexion`)
    
!!! info "Connexion impossible ou très faible réactivité"
    Si la connexion ne se fait pas ou si la réactivité est très lente, il ne faut pas hésiter à tester d'autres noms de machines parmi la liste suivante : `boursouf`, `boursouflet`, `cocatrix`, `trelawney`, `mcgonagall`, `joli-coeur`, `infini1`, `infini2`, `xeonphi`, `tesla`.
    
    __Si le problème ne se règle pas, cela vient sûrement du débit trop faible de votre connexion à Internet. Il faudra préferer travailler avec le terminal.__
    
!!! info "Se connecter en mode terminal"
        * Dans le rectangle *Type de session*, choisir `Application spécifique`
        * Dans le menu déroulant à côté du cadre blanc intitulé *Commande*, il faut choisir `Terminal`.
    

    
    
    
