# Le protocole SSH

__SSH__ (pour *Secure Shell*) est un protocole qui permet d'établir une __connexion sécurisée entre deux machines__ (ordinateurs, serveurs, ...).

Nous allons voir comment utiliser SSH pour accéder à distance à votre compte du CREMI.

## Pour débuter

Pour accéder au compte utilisateur `username` d'une machine distante dont le [nom de domaine](https://fr.wikipedia.org/wiki/Nom_de_domaine) est `serveur.fr`, il faut utiliser la commande suivante :
``` 
ssh username@serveur.fr
```
Le mot de passe du compte utilisateur est demandé pour authentifier la connexion. 

!!! info "Utilisation de l'adresse IP"
    Il est possible de spécifier l'[adresse IP](https://fr.wikipedia.org/wiki/Adresse_IP) de la machine distance au lieu de son nom de domaine.


### Se connecter à distance au CREMI

Le point d'entrée au CREMI se fait par la *passerelle* dont le nom de domaine est `sshproxy.emi.u-bordeaux.fr` (vous êtes connectés ensuite à une des machines de la liste : `jaguar`, `puma` ou `leopard`)

Supposons que vous êtes sur votre machine personnelle et que vous voulez vous connecter à votre compte au CREMI. D'après ce qui a été décrit précédemment, il faut taper la commande suivante :

``` 
ssh username@sshproxy.emi.u-bordeaux.fr
```
Vous devez ensuite entrer votre mot de passe et vous êtes maintenant authentifié.

!!! info "Confirmation d'accès à une machine de confiance"
	S’il s’agit de votre première connexion à la *passerelle* du CREMI, SSH va vous demander de confirmer qu’il s’agit bien d’une machine de confiance. Il faut taper `yes`.

!!! danger "Attention"
	Les machines `jaguar`, `puma` ou `leopard` sont des passerelles qui accueillent toutes les connexions entrantes au CREMI. 
	Pour cette raison, elles ne doivent pas être surchargées. Il ne faut donc jamais compiler des programmes, les exécuter ou lancer des applications depuis ces machines.


Après s'être connecté à la passerelle du CREMI, il faut se connecter à une des machines suivantes (ce sont des serveurs) si vous souhaitez travailler à distance au CREMI : `boursouf`, `boursouflet`, `cocatrix`, `trelawney`, `mcgonagall`, `joli-coeur`, `infini1`, `infini2`, `xeonphi`, `tesla`.

Par exemple, il faut taper la commande suivante pour se connecter à la machine ayant pour nom `boursouf` (votre mot de passe vous sera à nouveau demandé):
``` 
ssh username@boursouf
```
Puisque le nom d'utilisateur est le même que celui utilisé pour se connecter à la passerelle, vous pouvez simplement taper la commande :
``` 
ssh boursouf
```

__Vous pouvez maintenant exécuter des programmes, éditer des fichiers, compiler des projets ... comme si vous étiez sur une machine du CREMI__.

Pour se déconnecter d'une machine, il faut taper la commande `exit`.

---
!!! danger "Attention aux mots de passe erronnées"
    En cas plusieurs erreurs répétées au moment de saisir votre *mot de passe* ou *passphrase*, le CREMI va bannir votre adresse IP pendant une certaine durée !!! Vous ne pourrez plus vous connecter à distance.
---
???+ important "Accès à un PC du CREMI"
	Les serveurs du CREMI (les machines listées plus haut) sont normalement toujours allumés mais il n'y en a qu'une dizaine. 	
	Si vous souhaitez disposer d'une machine rien que pour vous, il vaut mieux dans ce cas [démarrer à distance un PC du CREMI](<https://services.emi.u-bordeaux.fr/nagios3/nagvis/nagvis/index.php>) avant de s'y connecter.
---	

## Pour utilisateur avancé
!!! info "A lire avant de continuer"
	Ce qui suit permet de *simplifier l'utilisation de SSH* mais peut-être un peu *complexe* à comprendre. Cependant, ces actions ne sont à effectuer qu'une seule fois pour correctement configurer votre machine.

### Connexion par paire de clés SSH

Afin de renforcer la sécurité de la connexion et d'éviter de devoir entrer son mot de passe à chaque fois, SSH permet l'utilisation d'un protocole cryptographique asymétrique s’appuyant sur une  __paire de clés privée/publique__. 
La clé privée restera toujours stockée sur votre machine, tandis que vous déposerez une copie de votre clé publique sur les machines à laquelles vous souhaitez vous connecter sans taper de mot de passe.

Avant de continuer, vérifier si vous possédez déjà une paire de clés dans un dossier `.ssh` devant se trouver à la racine de votre dossier utilisateur (home) sur votre machine (*il faut afficher les dossiers/fichiers cachés*). 

Sinon, nous allons créer une paire de clés protégée par une passphrase [(phrase de passe)](<https://fr.wikipedia.org/wiki/Phrase_secr%C3%A8te>), c'est à dire un mot de passe avec beaucoup de caractères. Pour cela, il faut taper la commande suivante :

``` bash
ssh-keygen -t rsa -b 4096
# Presser la touchée Entrée pour sauvegarder la paire de clés dans le répertoire proposé par défaut
# Entrer votre passphrase
```

La commande précédente génère deux fichiers : `id_rsa` (clé privée) et `id_rsa.pub` (clé publique).

Pour vérifier que les clés générées sont bien dans le répertoire `.ssh` situé dans votre répertoire utilisateur, il faut taper la commande suivante :

``` 
ls ~/.ssh
```

Il faut maintenant copier la clé publique sur la machine distante à laquelle on souhaite se connecter. On prend ci-dessous l'exemple d'une connexion à votre compte au CREMI *(il faut sinon remplacer `sshproxy.emi.u-bordeaux.fr` par le nom de domaine ou l'adresse IP de la machine distance)*.

	
=== "Linux et macOS"

    * Taper la commande suivante :
    ``` bash
    ssh-copy-id username@sshproxy.emi.u-bordeaux.fr
    ```
    Si la commande ne fonctionne pas, effectuer les commandes décrites pour Windows.
    

=== "Windows"
    * Exécuter la commande suivante (le mot de passe de votre compte au CREMI sera demandé):
    ``` bash
    # Ajoute la clé publique à la fin du fichier authorized_keys du dossier .ssh sur votre compte au CREMI (créé ce dossier s'il n'existe pas)
    cat ~/.ssh/id_rsa.pub | ssh username@sshproxy.emi.u-bordeaux.fr "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"   
    ```
    

---

!!! important 
    Par défaut, SSH demande d’entrer votre *passphrase* à chaque fois qu’il a besoin d’utiliser votre clé privée.
    Pour ne rentrer votre *passphrase* qu'une seule fois par session, il faut autoriser votre agent SSH à se charger en arrière-plan de l’accès à votre clé privée à chaque fois que le protocole SSH l’exigera.
    === "Linux et macOS"
	    Pour cela, il faut taper la commande :
	    ```
	    ssh-add  ~/.ssh/id_rsa
	    ```
    === "Windows"	    
        Sur Windows, il faut activer l'exécution automatique de l'agent SSH avant de taper la commande précédente [(documentation)](<https://docs.microsoft.com/fr-fr/windows-server/administration/openssh/openssh_keymanagement>). Pour cela, il faut exécuter le *Powershell* en tant qu'administrateur
        
        ![Powershell](images/Powershell.png)
        
        Il faut ensuite taper les commandes suivantes une à une :
        ```
        Get-Service -Name ssh-agent | Set-Service -StartupType Automatic
        Start-Service ssh-agent
        ssh-add  ~/.ssh/id_rsa
        ```
	Si la dernière commande génère une erreur du type "No such file or directory", il faut remplacer `~/.ssh/id_rsa` par le chemin absolu vers le ficher `id_rsa` (généralement `C:\Users\username\.ssh\id_rsa` où `username`est à remplacer par votre nom d'utilsateur *sur votre ordinateur personnel*)


### Utilisation d'un fichier de configuration

Pour éviter de se rapeller le nom complet des machines à laquelle on souhaite se connecter à distance, il est possible de définir des “alias” (i.e. pseudonymes pour les machines).
Pour cela, il faut créer un fichier de configuration appelé `config` *(sans aucune extension)* dans le répertoire `.ssh`. Copier le contenu ci-dessous dans le fichier créé. 


    ################# CREMI #################
    Host cremi
    Hostname sshproxy.emi.u-bordeaux.fr
    User username
    ForwardAgent yes
    
    Host boursouf
    Hostname boursouf.emi.u-bordeaux.fr
    User username
    ProxyJump cremi

Pour se connecter à la passerelle du CREMI, il suffit maintenant de taper la commande suivante dans votre terminal :
```
ssh cremi
```




Pour se connecter directement à la machine `boursouf` *sans passer par la passerelle du CREMI* (grâce à un rebond SSH permis par la ligne `ProxyJump`), il suffit maintenant de taper la commande suivante dans votre terminal :
```
ssh boursouf
```

Vous pouvez ajouter d'autre machines que `boursouf` dans le fichier `config`. Il suffit de copier les 4 dernières lignes et de changer le nom de la machine aux deux endroits où c'est nécessaire.




### Autres ressources

Si vous voulez approfondir, vous pouvez parcourir le [Guide
SSH](https://gforgeron.gitlab.io/ssh/guide.pdf) rédigé par l'UF info. Cet autre
[tutoriel](https://aurelien-esnard.emi.u-bordeaux.fr/ssh/tuto-ssh.pdf) peut également vous aider, et il est accompagné d'une [vidéo](https://www.youtube.com/watch?v=tjlhLdOqx38) explicative.



---
???+ note "Pour s'entraîner"
    Depuis votre machine personnelle, connectez-vous à une des machines de votre choix au CREMI.
    
    * Si la partie utilisateur avancé vous a semblé trop complexe : utilisez les commandes décrites dans [cette partie](#pour-debuter).
    
    * Sinon, utilisez une connexion par paire de clés SSH avec une *passphrase*. Référez-vous à cette  [cette partie](#pour-utilisateur-avance) *(vous devez préalablement générer vos clés SSH, les installer au CREMI, et créer un fichier de configuration)*.
---






