# Exercice

L'objectif de ce tutoriel est que vous soyez capable de réaliser certaines tâches sur votre compte au CREMI depuis votre machine personnelle (pour notamment avoir la même configuration que durant les TD machines).

Il y a plusieurs manières de se connecter au CREMI. [__ Les pages suivantes de ce tutoriel__](3-SSH.md) vous expliquent comment faire. Vous êtes libre de choisir la manière qui vous convient le mieux.

Pour vérifier si votre configuration fonctionne correctement, vous réaliserez les tâches suivantes en guise d'exercice:

1. Téléchargez le fichier suivant sur votre machine personnelle ([cliquer ici pour lancer le téléchargement](data/scriptPython.py))

2. Envoyez ce fichier intitulé `scriptPython.py` dans le répertoire racine de votre compte au CREMI

3. Connectez-vous à distance à votre compte au CREMI (à la passerelle d'abord puis à une des machines du CREMI)

4. Exécutez le fichier `scriptPython.py` en tapant la commande suivante 
    ``` python
    # Exécution du fichier python et l'affichage console est écrit directement dans un fichier resultat.txt
    python scriptPython.py > resultat.txt
    ``` 
5. Téléchargez le fichier `resultat.txt` sur votre machine. Il devrait contenir la phrase "Hello world". 


---

???+ info "Ressource additionnelle"
    [Documentation sur le travail à distance au CREMI](https://services.emi.u-bordeaux.fr/intranet/spip.php?article175) 
    
    


