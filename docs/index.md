# Introduction

Ce tutoriel vous explique comment vous pouvez travailler à distance sur votre compte au CREMI, notamment dans le cadre de projets et de TD machine.

Nous allons voir comment :

* échanger des fichiers entre votre machine (ordinateur) personnelle et votre compte au CREMI
* se connecter aux machines du CREMI pour exécuter des lignes de commandes, éditer des fichiers, développer des programmes (C, Python, ...), les compiler
  et les exécuter,  ...

En accédant aux machines du CREMI à distance, vous évitez les problèmes de compatibilité et l'installation d'outils/logiciels sur votre machine. 

Pour commencer le tutoriel, [cliquer ici](1-Prerequis)

!!! info "Information"
    A la suite de ce tutoriel, nous répondrons à vos questions lors d'un TD machine d'1h20.





---

## Responsables

* Aurélien Froger (aurelien.froger@u-bordeaux.fr)
* Lisl Weynans (lisl.weynans@u-bordeaux.fr)


## Remerciements

Merci à l'UF d'info et tout particulièrement à Aurélien Esnard, Laurent Reveillere et Pierre-Andre Wacrenier pour le partage de ressources (ce tutoriel s'inspire du niveau 3 du stage environnement proposé par l'UF info : <https://stage-env.gitlabpages.inria.fr/support/niveau3.html>)

