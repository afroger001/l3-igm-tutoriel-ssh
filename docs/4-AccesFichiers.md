# Accéder aux fichiers de votre compte au CREMI

Il existe plusieurs façons d'accéder aux fichiers de votre compte au CREMI et d'échanger des fichiers depuis ou vers votre machine personnelle.

## Depuis l'ENT

La première manière de faire requiert uniquement un navigateur Web.
Commencez par vous connecter à l'[ENT](https://ent.u-bordeaux.fr/). 
Puis sélectionnez le menu `Mon Bureau > Canal de Stockage`.
Patientez quelques secondes et vous devriez avoir accès au
fichiers de votre compte informatique du CREMI.

![image canal stockage](images/ENT-CanalStockage.png)

Vous pouvez naviguer dans l'arborescence de vos fichiers, télécharger un fichier
(bouton `Télécharger`) ou un répertoire entier sous forme d'une archive (bouton
`Zip`) et vous pouvez également créer de nouveau répertoire (bouton `Nv.
Dossier`), déplacer des fichiers (bouton `Copier/Couper/Coller`), ou encore
déposer un ou plusieurs fichiers de votre machine vers votre compte au CREMI (bouton
`Déposer`), etc.

???+ note "Pour s'entraîner"
    1. Télécharger un fichier de votre compte au CREMI vers votre ordinateur puis un dossier.
    2. Télécharger un fichier de votre ordinateur vers votre compte au CREMI.


## Par connexion SSH

### Avec le terminal

Pour transférer (envoyer ou télécharger) de façon sécurisée des fichiers entre votre machine et une machine distante, il faut utiliser la commande `scp` dans le __terminal de sa machine locale (ou personnelle)__. Cette commande s'appuie sur le protocole SSH.

??? info "Manuel de la commande `scp`" 
	Pour accéder au manuel de la commande `scp`, il faut taper la commande suivante : 
	```
	man scp
	```


Ci-dessous, on se place dans le cas où nous souhaitons transférer des fichiers avec une machine distante dont le nom de domaine est `serveur.fr` et notre nom d'utilisateur sur cette machine distante est `username`.

Par la suite, on appelle *répértoire courant*, le répertoire dans lequel on se situe dans le terminal (ce chemin apparaît à chaque ligne avant le curseur). Le répertoire dans lequel on se situe est aussi accessible par la commande `pwd`. Une autre commande utile est la commande `ls`permettant de lister tous les dossiers et fichiers présents dans le répertoire courant. On rappelle qu'il faut utiliser la commande `cd` pour se déplacer de répertoires en répertoires.


#### Envoi vers la machine distante

* __Envoyer des fichiers__

Pour envoyer un fichier vers la machine distante, il faut utiliser la commande suivante :
```
scp cheminLocalFichierAEnvoyer username@serveur.fr:cheminDistantFichierARecevoir
```

Par exemple, la commande suivante envoie le fichier `test.txt` du répertoire courant dans le dossier appelé `Projet` présent à la racine du compte de l'utilisateur sur la machine distante.
```
scp test.txt username@serveur.fr:~/Projet/
```

!!! info
    Vous devez entrer votre mot de passe pour vous authentifier à chaque envoi.
    
    Si vous avez configurer la connexion SSH par paire de clés (voir [cette page](../3-SSH/#connexion-par-cles-ssh)), alors vous n'aurez à entrer votre *passphrase* qu'au premier envoi.

!!! important "Utiliser un alias"
    Si vous avez créé un fichier de configuration (voir [cette page](../3-SSH/#utilisation-dun-fichier-de-configuration)), vous pouvez remplacer `username@serveur.fr` par l'alias (pseudonyme) utilisé.

Si on veut envoyer ce même fichier directement à la racine du compte de l'utilisateur sur la machine distante, on utilise la commande suivante : 
```
scp test.txt username@serveur.fr:~/
```

Si on veut envoyer deux fichiers `test.txt` et `document.txt` sur la machine distante, on utilise la commande suivante : 
```
scp test.txt document.txt username@serveur.fr:~/
```

La commande précédente fonctionne avec autant de fichiers que l'on souhaite.



* __Envoyer des dossiers__

Pour envoyer un dossier vers la machine distante, il faut utiliser la même commande que précédemment mais avec l'option `-r` (signifie _copie récursive_)
```
scp -r cheminLocalDossierAEnvoyer username@serveur.fr:cheminDistantDossierARecevoir
```

Par exemple, la commande suivante envoie le dossier `Test` à la racine du compte de l'utilisateur sur la machine distante :
```
scp -r Test username@serveur.fr:~/
```

#### Téléchargement vers sa machine locale

* __Télécharger des fichiers de la machine distance vers sa machine locale__

Pour télécharger un fichier présent sur la machine distance, il faut utiliser la commande suivante :
```
scp username@serveur.fr:cheminDistantFichierATelecharger cheminLocalFichierARecevoir 
```

Par exemple, la commande suivante télécharge le fichier `test.txt`, présent dans le dossier `Projet` se situant à la racine du compte de l'utilisateur sur la machine distante, vers le répertoire courant :
```
scp username@serveur.fr:Projet/test.txt ./

```
Pour télécharger ce même fichier dans le dossier `Travail` du répertoire courant :
```
scp username@serveur.fr:Projet/test.txt Travail

```

* __Télécharger un dossier__

Pour télécharger un dossier de la machine distante, il faut utiliser la même commande que précédemment avec l'option `-r` (signifie _copie récursive_)

```
scp -r username@serveur.fr:cheminDossierFichierATelecharger cheminDossierFichierARecevoir 
```

Par exemple, la commande suivante télécharge le dossier `Projet` se situant à la racine du compte de l'utilisateur sur la machine distante vers le répertoire courant :
```
scp -r username@serveur.fr:Projet ./

```
Pour télécharger ce même dossier dans le dossier `Travail` du répertoire courant :
```
scp -r username@serveur.fr:Projet ./Travail/
```


---
???+ note "Pour s'entraîner"
    1. Télécharger un fichier de votre compte au CREMI vers votre ordinateur puis un dossier en utilisant la commande `scp`.
    2. Télécharger un fichier de votre ordinateur vers votre compte au CREMI  en utilisant la commande `scp`.
    
    Dans chaque commande, pensez à remplacer `username` par votre login au CREMI.
---

### Avec l'outil FileZilla

FileZilla est un logiciel libre qui permet de connecter sa machine locale à une machine distante dans le but d’échanger des données.

1. Télécharger [FileZilla](https://filezilla-project.org/)
2. Lancer le logiciel FileZilla.
![image filezilla](images/FileZilla.png)
3. Ajouter un nouveau serveur en cliquant sur le bouton ![image filezilla](images/FIleZilla-AjoutServeur.png)
4. Cliquer sur `Nouveau site` et taper `CREMI`
5. Remplir les informations comme ci-dessous
	* Protocole : `SFTP - SSH File Transfer Protocol`
	* Hôte : `sshproxy.emi.u-bordeaux.fr`
	* Type d'authentification : `Interactif` *(si vous avez configurez une connexion SSH avec une paire de clés)*,  `Demander le mot de passe` *(sinon)*
	* Nom d'utilisateur : `username` ( ⚠️ __à remplacer par votre nom d'utilisateur CREMI__ ⚠️)
![image filezilla](images/FileZilla-InfoServeurCREMI.png)
6. Cliquer sur `Connexion` pour vous connecter au serveur CREMI
7. Vos dossiers sur le serveur CREMI apparaissent dans l'encadré à droite, tandis que les fichiers de votre ordinateur sont dans l'encadré à gauche.</br>
Pour envoyer des fichiers/dossiers de votre ordinateur, il faut faire un clic droit sur ceux-ci puis cliquer sur `Téléverser` (il faut se placer dans l'encadré de droite dans le dossier où vous souhaitez les envoyer)</br>
Pour récupérer sur votre ordinateur des fichiers/dossiers présent sur le serveur CREMI, il faut faire un clic droit sur ceux-ci puis cliquer sur `Télécharger` (il faut se placer dans l'encadré de gauche dans le dossier où vous souhaitez les récupérer)




