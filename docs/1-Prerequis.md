# Prérequis

Vous devez réaliser ce tutoriel/TD sur votre __ordinateur personnel__ à la maison. 

Votre ordinateur doit être __connecté à Internet__ par Wifi ou par cable Ethernet.

Il existe de nombreuses façons de travailler à distance, dépendant aussi de votre système d'exploitation : _Linux_, _Windows_, _macOS_... 
Nous avons essayé d'écrire un tutoriel adapté à un grand nombre de configurations, mais il n'est pas exclu que vous deviez parfois trouver vous-même une solution alternative du fait de votre configuration. 


Il faut __disposer d'un terminal__ (sous Linux, Windows ou macOS) qui permet d'interpréter des lignes de commandes. 

??? info "Utilisateur de Windows"
    Il y a plusieurs manières de disposer d'un terminal et d'un client SSH pour se connecter au CREMI :
    
    ![image windows terminals](images/windows-terminals-small.png)
        
    * utiliser le client SSH natif via *l'Invite de Commande* ou de préférence le *PowerShell* (pour vérifier si vous disposez d'un client SSH, il faut exécuter la commande `ssh`) 
    * utiliser un autre client SSH (à installer), comme par exemple  [Putty](https://www.putty.org/), [MobaXterm](https://mobaxterm.mobatek.net/) ou [X2Go](5-ModeBureau.md)
    * installer *WSL*, un sous-système Linux dans Windows 10 et ensuite utiliser la commande `ssh` dans un vrai terminal Linux.
    ??? info "Installation de WSL"
		Si vous disposez d'un ordinateur personnel sous Windows 10 (64 bits), il peut
		être intéressant pour vous d'installer un sous-système Linux (WSL) de type
		Ubuntu.

		Cette solution se base sur des techniques de virtualisation modernes ; elle est
		très confortable car elle permet de travailler dans un vrai système Linux sans
		avoir à mettre en place un *dual boot*. Néanmoins, son installation est un peu
		plus compliquée et elle nécessite de posséder une machine suffisament puissante.

		Pour installer WSL (version 2, de préférence), vous pouvez suivre ces tutoriels :

		* <https://docs.microsoft.com/fr-fr/windows/wsl/install-win10>
		* <https://korben.info/installer-wsl2-windows-linux.html>

		Choisissez par exemple d'installer un sous-système Ubuntu pour
		disposer d'un configuration similaire au CREMI (cf. [Microsoft
		Store](https://aka.ms/wslstore))

		Puis installer dans votre sous-système Linux les paquets nécessaires, comme par 			exemple le client `ssh`, le compilateur `gcc`, le débogeur `gdb`, `make`, etc :

		```
		sudo apt-get update
		sudo apt-get install ssh gcc gdb build-essential
		```

		Depuis l'explorateur de fichier Windows, il est possible d'accéder aux fichiers
		du sous-système Linux en tapant ce chemin `\\wsl$`, et réciproquement vous
		pouvez accéder aux fichiers Windows depuis WSL directement à partir de `/mnt/c`.

		**Attention** : Il est nécessaire pour faire fonctionner WSL d'activer la
		virtualisation dans le BIOS, ainsi que Hyper-V dans Panneau Configuration /
		Windows Feature. Vous pouvez vérifier qu'un *hyperviseur* est installé avec la
		commande *systeminfo* (sous PowerShell en mode Administrateur).



    


Il faut __disposer d'un client SSH__. Pour cela, il suffit d'exécuter la commande `ssh` dans votre terminal. Vous devez voir apparaître à l'écran un affichage qui ressemble à celui présenté ci-dessous
  
  
```
usage: ssh [-46AaCfGgKkMNnqsTtVvXxYy] [-B bind_interface]
           [-b bind_address] [-c cipher_spec] [-D [bind_address:]port]
           [-E log_file] [-e escape_char] [-F configfile] [-I pkcs11]
           [-i identity_file] [-J [user@]host[:port]] [-L address]
           [-l login_name] [-m mac_spec] [-O ctl_cmd] [-o option] [-p port]
           [-Q query_option] [-R address] [-S ctl_path] [-W host:port]
           [-w local_tun[:remote_tun]] destination [command]
```
  
!!! Important 
	A chaque ouverture du terminal, placez-vous à la racine de votre dossier utilisateur en tapant la commande suivante :
	```
	cd ~ 
	```
  
[__ Cliquer ici après avoir vérifié les pré-requis__](2-Exercice.md)

